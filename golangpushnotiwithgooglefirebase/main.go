package main

import (
	Controllers "api/controllers"
	env "api/env"
	"fmt"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

func main() {

	switch os.Getenv("ENV") {
	case "pro":
		fmt.Println("Pro")
		env.SetDevEnv()
	default:
		fmt.Println("dev")
		env.SetDevEnv()
	}

	app := fiber.New()
	app.Use(cors.New())
	app.Use(cors.New(cors.Config{
		Next:             nil,
		AllowOrigins:     "*",
		AllowMethods:     "GET,POST,HEAD,PUT,DELETE,PATCH",
		AllowHeaders:     "*",
		AllowCredentials: false,
		ExposeHeaders:    "",
		MaxAge:           0,
	}))

	app.Use(logger.New(logger.Config{
		Format:     "${blue} ${time}  ${red} ${latency} ${cyan} ${method} ${path} ${yellow} ${status} ${green} ${ip} ${ua}${resetColor}\n",
		TimeFormat: "02-Jan-2006",
		TimeZone:   "Asia/Bangkok",
		Output:     os.Stdout,
	}))

	app.Get("/", func(c *fiber.Ctx) error {

		return c.JSON(fiber.Map{"a": "s"})
	})
	api_web_notification := app.Group("/webnotification") // /api
	api_web_notification.Post("/noti", Controllers.Web_Notification_Controller)

	app.Listen(":5000")
}
