module api

go 1.15

require (
	firebase.google.com/go/v4 v4.2.0
	github.com/gofiber/fiber/v2 v2.3.3
	google.golang.org/api v0.38.0
)
