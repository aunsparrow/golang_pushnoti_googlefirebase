package controllers

import (
	req_models "api/models"
	"context"
	"fmt"

	firebase "firebase.google.com/go/v4"
	"firebase.google.com/go/v4/messaging"
	"github.com/gofiber/fiber/v2"
	"google.golang.org/api/option"
)

func Web_Notification_Controller(c *fiber.Ctx) error {
	req := new(req_models.Web_pushnoti_request_model)
	if err := c.BodyParser(req); err != nil {
		fmt.Println(err)
	}
	//logic < 100 device
	var registrationTokens []string
	var startLen int
	var endLen int
	_ = startLen
	_ = endLen
	registrationTokens = req.Token
	if len(req.Token) > 90 {
		//device > 90
		for i := 0; i < (len(req.Token)/90)+1; i++ {
			startLen = i * 90
			endLen = (i + 1) * 90

			if startLen == (len(req.Token)/90)*90 {
				endLen = len(req.Token)
				registrationTokens = req.Token[startLen:endLen]
				Push_noti(registrationTokens)
			} else {
				registrationTokens = req.Token[startLen:endLen]
				Push_noti(registrationTokens)
			}

		}
	}
	Push_noti(registrationTokens)
	return c.Status(fiber.StatusOK).SendString("test")
}

func Push_noti(registrationTokens []string) {
	ctx := context.Background()
	opt := option.WithCredentialsFile("./configs/golangnoti-firebase-adminsdk-1gf12-e9262a6c08.json")
	app, err := firebase.NewApp(ctx, nil, opt)

	if err != nil {
		fmt.Println("error initializing app: %v", err)
	}

	configs := &messaging.WebpushConfig{}
	configs.Data = map[string]string{
		"score": "850",
		"time":  "2:45",
	}

	noti := &messaging.WebpushNotification{}
	noti.Title = "testSend message"
	noti.Body = "test test"
	noti.Icon = "https://file.job.thai.com/prakad/rmutt202003/rmutt202003_26"
	configs.Notification = noti
	//messageing

	message := &messaging.MulticastMessage{
		Data: map[string]string{
			"score": "850",
			"time":  "2:45",
		},
		Webpush: configs,
		Tokens:  registrationTokens,
	}

	client, err := app.Messaging(ctx)
	if err != nil {
		fmt.Println("error getting Messaging client: %v\n", err)
	}

	br, err := client.SendMulticast(ctx, message)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("%d messages were sent successfully\n", br.SuccessCount)
}
