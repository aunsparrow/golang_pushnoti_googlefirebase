package env

import "os"

// SetDevEnv is Init Env For Dev
func SetDevEnv() {
	os.Setenv("ENV", "dev")
	os.Setenv("POSTGRES_PASSWORD", "a98cAcVD32h2JpfYAaR9dQeQRw2")
	os.Setenv("POSTGRES_USER", "postgres")
	os.Setenv("POSTGRES_PORT", "5433")
	os.Setenv("POSTGRES_HOST", "203.150.34.156")
	os.Setenv("POSTGRES_DATABASENAME", "dev_onechat_bot_auto_reply")
}
